(function(){
	var app = angular.module('poppyProjectEditor', ['rest-api','ui.layout', 'ui.tree', 'ng-context-menu', 'ngResource']);

    app.controller("PoppyMainController",['$scope', '$http', function($scope, $http) {
        
        $scope.model = {
            project: [],
            projects: [],
            pageTree: [],
            siteMap: [],
            elements: []
        };

        $http.get('/poppy-frontend/data/html-elements.json').success(function(adata){
            $scope.model.elements = adata; 
        });     
    }]);

    app.controller("PoppyProjectController",['$scope', '$http', function($scope, $http) {
        
        $scope.openProject = function(id) {
            $scope.model.project = id;
            console.log("opened project "+ id);
        };
        
        $scope.deleteProject = function(id) {
            console.log("deleted project "+ id);
        };
        
        $scope.listAllProjects = function(){
            $http.get('/poppy-frontend/data/projects-data.json').success(function(adata){
                $scope.model.projects = adata;
            });
        };
        
    }]);

    app.controller("PageTreeController",['$scope', '$http', function($scope, $http) {
    $scope.tagName = "";
        $scope.loadProject = function(obj){
            /**
             * Testing reload of datascource - if function gets called with content object
             * it will reinit $scope.list
             * turns out that reinitializing the scope.list var auto-updates the treeView (jea!)
             */
            if(!obj){
                $http.get('/poppy-frontend/data/tree-data.json').success(function(adata){
                    $scope.model.pageTree = adata;
                    $scope.list =  $scope.model.pageTree;  
                });
            }else{  
                $scope.list = $scope.model.pageTree = [{"id":2143, "title": "myTitle", "tagName": "myTitle", "items":[]}];
                console.log("hu");
            }
        };
        
        //Should probably be a service like the one in SitemapController
        $scope.newSubItem = function(scope, tagName) {
            var nodeData = scope.$modelValue;
            var name = tagName;
            
            //Basically what we need to create new element
            console.log("new element type: "+name+" in object "+ nodeData.id);
            
            nodeData.items.push({
                parentId: nodeData.id,
                tagName: name,
                id: nodeData.id * 10 + nodeData.items.length,
                title: nodeData.title + '.' + (nodeData.items.length + 1),
                items: []
            });
        }; 
        
    }]);

    app.controller('SitemapController', ['$scope', '$http', function($scope, $http) {

        $http.get('/poppy-frontend/data/sitemap-data.json').success(function(bdata){
            $scope.model.siteMap = bdata;
            $scope.links = $scope.model.siteMap;
        });
        
        //Should probably be a service, too
        $scope.selectElement = function(scope) {
            var nodeData = scope.$modelValue;
            console.log(scope);
            console.log(nodeData.id);
        };
        
        //Should probably be a service like the one in pageTreeController
        $scope.newSubItem = function(scope) {
            var nodeData = scope.$modelValue;
            nodeData.items.push({
                parentId: nodeData.id,
                tagName:"gen",
                id: nodeData.id * 10 + nodeData.items.length,
                title: nodeData.title + '.' + (nodeData.items.length + 1),
                items: []
            });
        };
    }]);

    

    
    app.filter('jsonToHtml', ['$sce', function($sce) {
        return function(input) {
            input = input || '';
            var out = "";
            out = buildHtml(input, out);
            return $sce.trustAsHtml(out);
        };
    }]);
    
    function buildHtml(input, out) {
        
        for (var i = 0; i < input.length; i++) {
            var text = "";
            var attributes = "";
                
            if(input[i].text)  text =  input[i].text;
            if(input[i].attributes)  attributes =  input[i].attributes;
            
            out += "<"+input[i].tag+" "+attributes+" class='gen'>"+input[i].id+" "+text; 
            if(input[i].items.length){
                out = buildHtml(input[i].items, out);
            }
            input[i].tag = undefined;
            out += "</"+input[i].tag+">";
        }
        return out;
    }
})();
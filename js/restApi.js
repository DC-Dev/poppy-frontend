(function(){
    var app = angular.module('rest-api', ['ngResource']);

    app.factory("Answer", function($resource) {
        return $resource("/projects/testApi", {}, {
            query: { method: "GET", isArray: true }
        });
    });


    app.controller("RestApiController", function($scope, Answer) {
        $scope.loader = "Loading from server... Server responses after a manual timeout of 5 seconds to demonstrate asynchronous calls.";
        $scope.loader =  "/media/ajax-pulse.gif";
        Answer.query(function(data) {
            $scope.loader = "";
            $scope.apiResult = data;
        });
    });
    
})();